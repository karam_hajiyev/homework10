package az.ibatech.model;

import az.ibatech.model.Human;

public final class Man extends Human {
    // constructors
    public Man() {
    }
    public Man(String name, String surname, int y, int m, int d) {
        super(name, surname, y, m, d);
    }
    public Man(String name, String surname, int y, int m, int d, int iq) {
        super(name, surname, y, m, d, iq);
    }

    // methods
    public void repairCar() {
        System.out.println("It's time to repair car.");
    }
    public void greetPet() {
        System.out.println("Hello, " + getFamily().getPet().getNickname() + '.');
    }
}

