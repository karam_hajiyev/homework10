package az.ibatech.model;


public final class Woman extends Human {
    // constructors
    public Woman() {
    }

    public Woman(String name, String surname, int y, int m, int d) {
        super(name, surname, y, m, d);
    }

    public Woman(String name, String surname, int y, int m, int d, int iq) {
        super(name, surname, y, m, d, iq);
    }

    // methods
    public void makeUp() {
        System.out.println("It's time to go to the beautician.");
    }

    public void greetPet() {
        System.out.println("Hello, my sweeties " + getFamily().getPet().getNickname() + '.');
    }
}

